#include "config.h"

#include <libopencm3/cm3/cortex.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/nvic.h>
#include "onewire.h"

uint16_t onewire_gotlen = 0;
// array for sending/receiving data
uint8_t ow_data_array[10];
void (*ow_process_resdata)() = 0;
extern char text[];

void onewire_init()
{
  gpio_set_mode(GPIO_BANK_TIM3_CH3, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO_TIM3_CH3);
  gpio_set(GPIO_BANK_TIM3_CH3, GPIO_TIM3_CH3);

  rcc_periph_clock_enable(RCC_TIM3);

  timer_reset(TIM3);
  timer_set_mode(TIM3, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  // Timer frequency is autocorrected when DIV2 is enabled to APB1, 
  // so for F103 timer frequency is equals to AHB freq for all known cases.
  timer_set_prescaler(TIM3, (rcc_ahb_frequency / 1000000)-1); // 1MHz
  timer_enable_preload(TIM3);
  timer_one_shot_mode(TIM3);
  timer_enable_break_main_output(TIM3);

  /* 
   * Configure output compare for probe 2
   * Output configured for TIM3CH3 
   * Low, when CNTR < CC1,  high - otherwise
   */ 
  timer_set_oc_mode(TIM3, TIM_OC3, TIM_OCM_PWM2);
  timer_set_oc_polarity_high(TIM3, TIM_OC3);
  timer_enable_oc_output(TIM3, TIM_OC3);
   /*
    * Configure input capture for probe 2
    * Input configured for TI4FP3
    * Trigger - Rising
    */
  timer_ic_set_input(TIM3, TIM_IC4, TIM_IC_IN_TI3);
  // There is no related call in library
  // Call is similar to timer_set_oc_polarity_high(TIM3, TIM_OC4);
  TIM_CCER(TIM3) &= ~TIM_CCER_CC4P;
  timer_ic_enable(TIM3, TIM_IC4);
}


void ow_wait(uint16_t len)
{
    timer_set_period(TIM3, len);
    timer_disable_break_main_output(TIM3);
    gpio_set(GPIO_BANK_TIM3_CH3, GPIO_TIM3_CH3);
  
    timer_generate_event(TIM3, TIM_EGR_UG);
    timer_enable_counter(TIM3);
    while(OW_BUSY);
    timer_enable_break_main_output(TIM3);
}
// Zero result means no capture occured
uint16_t ow_pulse(uint16_t oc, uint16_t len)
{
  timer_set_period(TIM3, len);
    timer_set_oc_value(TIM3, TIM_OC3, oc);
    timer_generate_event(TIM3, TIM_EGR_UG);
    timer_enable_counter(TIM3);
    while(OW_BUSY);
    if (timer_get_flag(TIM3, TIM_SR_CC4IF)){
      return TIM_CCR4(TIM3);
    }else{
      return 0;
    }
}
uint8_t onewire_reset()
{
  return ow_pulse(RESET_P, RESET_LEN) > RESET_BARRIER;
}
void ow_send_bit(uint8_t v)
{
  ow_pulse((v&1) ? BIT_ONE_P : BIT_ZERO_P ,BIT_LEN);
}
uint8_t ow_receive_bit()
{
  uint16_t r = ow_pulse(BIT_READ_P, BIT_LEN);
  return ((r > 0) && (r < ONE_ZERO_BARRIER));
}

void ow_send_byte(uint8_t v) {
  uint8_t i;

  for (i=0; i<8; i++) {
    ow_send_bit(v & 1);
    v >>= 1;
  }
}

void ow_send_bytes(const uint8_t *buf, uint16_t count) {
  uint16_t i;
  for (i = 0 ; i < count ; i++)
    ow_send_byte(buf[i]);
}

uint8_t ow_receive_byte() {
  uint8_t i;
  uint8_t r = 0;

  for (i = 0; i<8; i++) {
    r >>= 1;
    if (ow_receive_bit()) 
      r |= 0x80;
  }
  return r;
}

void ow_receive_bytes(uint8_t *buf, uint16_t count) {
  uint16_t i;
  for (i = 0 ; i < count ; i++)
    buf[i] = ow_receive_byte();
}
void ow_select(uint8_t rom[8])
{
  uint8_t i;

  ow_send_byte(OW_MATCH_ROM);           // Choose ROM
  for(i=0; i<8; i++) ow_send_byte(rom[i]);
}

void ow_skip()
{
  ow_send_byte(OW_SKIP_ROM);           // Skip ROM
}

uint8_t ow_crc8( uint8_t *addr, uint8_t len)
{
  uint8_t crc = 0;
  uint8_t i,j;
  uint8_t valid = 0;

  for(i=0; i<len; i++){
    uint8_t inbyte = addr[i];
    valid |= inbyte;
    for (j = 0; j<8; j++) {
      uint8_t mix = (crc ^ inbyte) & 0x01;
      crc >>= 1;
      if (mix) crc ^= 0x8C;
      inbyte >>= 1;
    }
  }
  if (!valid) return 0xff;
  return crc;
}
/**
 * convert temperature from ow_data_array (scratchpad)
 * in case of error return 200000 (ERR_TEMP_VAL)
 * return value in 1000th degrees centigrade
 * don't forget that bytes in ow_data_array have reverce order!!!
 * so:
 * 0 8 - themperature LSB
 * 1 7 - themperature MSB (all higher bits are sign)
 * 2 6 - T_H
 * 3 5 - T_L
 * 4 4 - B20: Configuration register (only bits 6/5 valid: 9..12 bits resolution); 0xff for S20
 * 5 3 - 0xff (reserved)
 * 6 2 - (reserved for B20); S20: COUNT_REMAIN (0x0c)
 * 7 1 - COUNT PER DEGR (0x10)
 * 8 0 - CRC
 */
int16_t gettemp(){
	// detect DS18S20
	int16_t t = 0;
	uint8_t l,m;
	int8_t  v;
        // Check for CRC8
        if (ow_crc8(ow_data_array, 9))
          return ERR_TEMP_VAL;

	if(ow_data_array[7] == 0xff) // 0xff can be only if there's no such device or some other error
		return ERR_TEMP_VAL;
	m = ow_data_array[1];
	l = ow_data_array[0];
	if(ow_data_array[4] == 0xff){ // DS18S20
		v = l >> 1 | (m & 0x80); // take signum from MSB
		t = ((int16_t)v) * 10;
		if(l&1) t += 5L; // decimal 0.5
	}
	else{
		v = l>>4 | ((m & 7)<<4);
		if(m&0x80){ // minus
			v |= 0x80;
		}
		t = ((int16_t)v) * 10;
		m = l&0x0f; // add decimal
		t += (int16_t)m; // t = v*10 + l*1.25 -> convert
		if(m > 1) t++; // 1->1, 2->3, 3->4, 4->5, 4->6
		else if(m > 5) t+=2; // 6->8, 7->9
	}
	return t;
}

int16_t ow_get_temp(uint8_t mode){

  gpio_set(GPIO_BANK_TIM3_CH3, GPIO_TIM3_CH3);
  if(!onewire_reset()){
    return ERR_TEMP_VAL;
  }

if (!mode){

  ow_data_array[0] = OW_SKIP_ROM;
  ow_data_array[1] = OW_CONVERT_T;
  ow_send_bytes(ow_data_array, 2);

  return ERR_TEMP_VAL;
}else{

//  ow_data_array[0] = OW_MATCH_ROM;
  ow_data_array[0] = OW_SKIP_ROM;
  ow_data_array[1] = OW_READ_SCRATCHPAD;
  ow_send_bytes(ow_data_array, 2);
  ow_receive_bytes(ow_data_array, 9);
  return gettemp();
  }
}

uint8_t ow_get_id(uint8_t* buf){
  if(!onewire_reset()){
    return 0;
  }
  ow_data_array[0] = OW_READ_ROM;
  ow_send_bytes(ow_data_array, 1);
  ow_receive_bytes(buf, 8);
  if (ow_crc8(buf, 8))
    return 0;
  return 1;
}
