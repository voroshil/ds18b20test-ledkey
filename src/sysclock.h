#ifndef _H_SYSCLOCK_H_
#define _H_SYSCLOCK_H_

// Using downcounter1 for delays in dialer
#define BUTTON_DOWNCOUNTER downcounter[0]
#define STATUS_LED_DELAY_DOWNCOUNTER downcounter[1]
#define PHONE_CALL_DOWNCOUNTER downcounter[2]
#define PBS_DETECTOR_DOWNCOUNTER downcounter[3]
#define AMP_CALIBRATION_DOWNCOUNTER downcounter[4]
#define DEBUG_DOWNCOUNTER downcounter[5]
#define LCD_DOWNCOUNTER downcounter[6]
#define SECONDS_DOWNCOUNTER downcounter[7]
#define TEMP_PROBE_DOWNCOUNTER downcounter[8]

#define SYSCLOCK_MAX_COUNTER 9

extern uint16_t downcounter[];

extern uint32_t seconds;

void sysclock_init();

#endif // _H_SYSCLOCK_H_