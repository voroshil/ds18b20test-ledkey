#ifndef _H_LCD_TM1638_H_
#define _H_LCD_TM1638_H_

#define DISC0 0
#define DISC1 1
#define DISC2 2
#define DISC3 3
#define DISC4 4
#define DISC5 5
#define DVD   6
#define DDD   7
#define MP3   8
#define MP4   9
#define VCD   10
#define PLAY  11
#define PAUSE 12
#define DOTS1 13
#define DOTS2 14
#define DTS   15
#define B1    16
#define C1    17

void lcd_init();
void lcd_test();
void lcd_update();
void lcd_disc_on(uint8_t led);
void lcd_disc_off(uint8_t led);
void lcd_disc_all(uint8_t on);
void lcd_set_char(uint8_t addr, char chr);
void lcd_set_seg(uint8_t addr, uint8_t num);
void lcd_apply_font(uint8_t glyph, uint8_t addr);

#endif // _H_LCD_TM1638_H_