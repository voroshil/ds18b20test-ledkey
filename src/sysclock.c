#include "config.h"

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/systick.h>

#include "sysclock.h"

uint16_t downcounter[SYSCLOCK_MAX_COUNTER];

uint32_t seconds = 0;

void sys_tick_handler(void)
{
  uint8_t i;
  seconds++;

  for(i=0; i<SYSCLOCK_MAX_COUNTER;i++){ 
    if (downcounter[i]) downcounter[i]--;
  }
}

void sysclock_init()
{
  uint8_t i;
  for(i=0; i<SYSCLOCK_MAX_COUNTER;i++){
    downcounter[i] = 0;
  }
  systick_set_frequency(1000, rcc_ahb_frequency);
  systick_interrupt_enable();
  systick_counter_enable();
}

