#include "config.h"

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/cortex.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#include "onewire.h"
#include "sysclock.h"
#include "lcd_ledkey.h"

#define  DISC0 0

#define MAX_PROBES 8
uint16_t in_temp = ERR_TEMP_VAL;

#define DISPLAY_MODE_TEMP 0
#define DISPLAY_MODE_ID_0_3  1
#define DISPLAY_MODE_ID_4_7  2
#define DISPLAY_LAST_MODE    3

uint8_t display_mode = DISPLAY_MODE_TEMP;
uint8_t probe_id[8];
uint8_t valid_id = 0;
uint8_t switch_mode_pressed = 0;

void process_temp_probe(){
  static uint8_t mode = 0;

  if (TEMP_PROBE_DOWNCOUNTER){
    return;
  }

  switch(mode){
    case 0:
      if (!ow_get_id(probe_id)){
        valid_id = 0;
      }else{
        valid_id = 1;
      }
      mode = 1;
      break;
    case 1:
      ow_get_temp(0);
      TEMP_PROBE_DOWNCOUNTER = 900;
      mode = 2;
      break;
    case 2:
      in_temp  = ow_get_temp(1);
      mode = 0;
      TEMP_PROBE_DOWNCOUNTER = 500;
      break;
  }
}

void display_id(uint8_t high){
  lk_led_off(14);
  if(!valid_id){
    lk_set_char(0, '-');
    lk_set_char(1, '-');
    lk_set_char(2, '-');
    lk_set_char(3, '-');
    lk_set_char(4, '-');
    lk_set_char(5, '-');
    lk_set_char(6, '-');
    lk_set_char(7, '-');
  }else if (!high){
    lk_set_seg(0, probe_id[0] >> 4);
    lk_set_seg(1, probe_id[0] &0xf);
    lk_set_seg(2, probe_id[1] >> 4);
    lk_set_seg(3, probe_id[1] &0xf);
    lk_set_seg(4, probe_id[2] >> 4);
    lk_set_seg(5, probe_id[2] &0xf);
    lk_set_seg(6, probe_id[3] >> 4);
    lk_set_seg(7, probe_id[3] &0xf);
  }else{
    lk_set_seg(0, probe_id[4] >> 4);
    lk_set_seg(1, probe_id[4] &0xf);
    lk_set_seg(2, probe_id[5] >> 4);
    lk_set_seg(3, probe_id[5] &0xf);
    lk_set_seg(4, probe_id[6] >> 4);
    lk_set_seg(5, probe_id[6] &0xf);
    lk_set_seg(6, probe_id[7] >> 4);
    lk_set_seg(7, probe_id[7] &0xf);
  }
}
void display_temp(){
  int16_t t;

  lk_set_char(0,' ');
  lk_set_char(1,' ');
  lk_set_char(2,' ');
  lk_set_char(3,' ');

  if (in_temp == ERR_TEMP_VAL){
    lk_led_off(DISC0);


    lk_set_char(4,'-');
    lk_set_char(5,'-');
    lk_set_char(6,'-');
    lk_set_char(7,'-');
    lk_led_off(14);
  }else if (in_temp != ERR_TEMP_VAL){
    lk_led_on(DISC0);
    lk_led_on(14);

    t = in_temp;
    if (t <= -100){
      lk_set_char(4, '-');
      t = -t;
      lk_set_seg(5, (t / 100) % 10);
      lk_set_seg(6, (t/10) % 10);
      lk_set_seg(7, t % 10);
    }else if (t < 0){
      lk_set_char(4, ' ');
      lk_set_char(5, '-');
      t = -t;
      lk_set_seg(6, (t/10) % 10);
      lk_set_seg(7, t % 10);
    }else if (t >= 1000){
      lk_set_seg(4, (t / 1000) % 10);
      lk_set_seg(5, (t / 100) % 10);
      lk_set_seg(6, (t / 10) % 10);
      lk_set_seg(7, t % 10);
    }else if (t >= 100){
      lk_set_char(4, ' ');
      lk_set_seg(5, (t / 100) % 10);
      lk_set_seg(6, (t / 10) % 10);
      lk_set_seg(7, t % 10);
    }else if (t >= 10){
      lk_set_char(4, ' ');
      lk_set_char(5, ' ');
      lk_set_seg(6, (t / 10) % 10);
      lk_set_seg(7, t % 10);
    }else{
      lk_set_char(4, ' ');
      lk_set_char(5, ' ');
      lk_set_seg(6, 0);
      lk_set_seg(7, t % 10);
    }
  }
}

void process_lcd(){
  uint8_t btn;
  if (LCD_DOWNCOUNTER){
    return;
  }
  btn = lk_get_buttons();
  if (btn & 1){
    switch_mode_pressed = 1;
  } else if (switch_mode_pressed){
    display_mode = (display_mode + 1 ) % DISPLAY_LAST_MODE;
    switch_mode_pressed = 0;
  }
  switch(display_mode){
    case DISPLAY_MODE_TEMP:
      lk_led_on(4);
      lk_led_off(5);
      lk_led_off(6);
      display_temp();
      break;
    case DISPLAY_MODE_ID_0_3:
      lk_led_off(4);
      lk_led_on(5);
      lk_led_off(6);
      display_id(0);
      break;
    case DISPLAY_MODE_ID_4_7:
      lk_led_off(4);
      lk_led_off(5);
      lk_led_on(6);
      display_id(1);
      break;
    default:
      lk_clear();
      break;
  } 
  lk_update();
  LCD_DOWNCOUNTER = 200;
}

void update_status_led(){
   if (!STATUS_LED_DELAY_DOWNCOUNTER){
     gpio_toggle(GPIOC, GPIO13);
     STATUS_LED_DELAY_DOWNCOUNTER = 500;
   }
}
static uint32_t lk_receive_cb(void){
  uint32_t temp = 0;

  // Pull-up on
  gpio_set_mode(GPIO_BANK_SPI2_MOSI, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_SPI2_MOSI);
  gpio_set(GPIO_BANK_SPI2_MOSI, GPIO_SPI2_MOSI);

  for (int i = 0; i < 32; i++) {
    temp >>= 1;

    gpio_clear(GPIO_BANK_SPI2_SCK, GPIO_SPI2_SCK);

    if (gpio_get(GPIO_BANK_SPI2_MOSI, GPIO_SPI2_MOSI)){
      temp |= 0x80000000;
    }

    gpio_set(GPIO_BANK_SPI2_SCK, GPIO_SPI2_SCK);
  }

  // Pull-up off
  gpio_set_mode(GPIO_BANK_SPI2_MOSI, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO_SPI2_MOSI);
  gpio_clear(GPIO_BANK_SPI2_MOSI, GPIO_SPI2_MOSI);
  return temp;
}
static void lk_send_cb(uint8_t data){
  uint8_t i;
  for(i=0; i<8; i++){
    gpio_clear(GPIO_BANK_SPI2_SCK, GPIO_SPI2_SCK);
    if (data & 1){
      gpio_set(GPIO_BANK_SPI2_MOSI, GPIO_SPI2_MOSI);
    }else{
      gpio_clear(GPIO_BANK_SPI2_MOSI, GPIO_SPI2_MOSI);
    }
    gpio_set(GPIO_BANK_SPI2_SCK, GPIO_SPI2_SCK);
    data >>= 1;
  }
}
static void lk_ss_cb(uint8_t ss){
  if (ss){
    gpio_set(GPIO_BANK_SPI2_NSS, GPIO_SPI2_NSS);
  }else{
    gpio_clear(GPIO_BANK_SPI2_NSS, GPIO_SPI2_NSS);
  }
}
void lk_delay(void){
  for(int i=0; i<1000000; i++){};
}

void lk_hw_init()
{ 
  gpio_set_mode(GPIO_BANK_SPI2_NSS, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO_SPI2_NSS);
  gpio_set(GPIO_BANK_SPI2_NSS, GPIO_SPI2_NSS);

#ifdef USE_LCD_SPI
  gpio_set_mode(GPIO_BANK_SPI2_SCK, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_SPI2_SCK);
  gpio_set_mode(GPIO_BANK_SPI2_MOSI, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_SPI2_MOSI);

  spi_init_master(SPI2, SPI_CR1_BAUDRATE_FPCLK_DIV_8, SPI_CR1_CPOL_CLK_TO_1_WHEN_IDLE,
			SPI_CR1_CPHA_CLK_TRANSITION_2, SPI_CR1_DFF_8BIT,
			SPI_CR1_LSBFIRST);
  
  spi_set_bidirectional_mode(SPI2);
  spi_enable_software_slave_management(SPI2);
  spi_set_nss_high(SPI2);
  spi_enable(SPI2);

#else
   gpio_set_mode(GPIO_BANK_SPI2_SCK, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO_SPI2_SCK);
   gpio_set_mode(GPIO_BANK_SPI2_MOSI, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO_SPI2_MOSI);
  
   gpio_set(GPIO_BANK_SPI2_SCK, GPIO_SPI2_SCK);
   gpio_set(GPIO_BANK_SPI2_MOSI, GPIO_SPI2_MOSI);
#endif

}
int main(void){

  rcc_clock_setup_in_hse_8mhz_out_72mhz();
  rcc_set_usbpre(RCC_CFGR_USBPRE_PLL_CLK_DIV1_5);

  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);

  sysclock_init();
  lk_hw_init();
  lk_init(lk_receive_cb, lk_send_cb, lk_ss_cb);
  lk_test(lk_delay);

  gpio_set(GPIOC, GPIO13);
  onewire_init();
//  gpio_clear(GPIOC, GPIO13);


  cm_enable_interrupts();

  while(1) {
    update_status_led();
    process_temp_probe();
    process_lcd();
 }
  return 0;
}