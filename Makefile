TARGET = cprobe
SDIR := src

CFLAGS += -DSTM32F1

Q := @

INCLUDES += libopencm3/include
LIB_DIRS += libopencm3/lib

SOURCEDIRS += $(SDIR) 

OBJS += $(patsubst %.c, %.o, $(wildcard $(addsuffix /*.c, $(SOURCEDIRS))))

LDSCRIPT = $(TARGET).ld

PREFIX ?= arm-none-eabi
CC = $(PREFIX)-gcc
LD = $(PREFIX)-gcc
SZ = $(PREFIX)-size
OBJCOPY = $(PREFIX)-objcopy

LIBNAME = opencm3_stm32f1
LDLIBS = -l$(LIBNAME) 

LDLIBS += -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group -mthumb
LDLIBS += -nostartfiles
LDLIBS += -Wl,--gc-sections

LDFLAGS	+= -Wl,-Map=$(TARGET).map

CFLAGS += -mcpu=cortex-m3 -mthumb -fno-builtin
CFLAGS += -pipe -Wall -Os
CFLAGS += $(addprefix -I, $(INCLUDES))

AFLAGS = -mcpu=cortex-m3 -mthumb
LDFLAGS += -static -T$(LDSCRIPT)
LDFLAGS += $(addprefix -L, $(LIB_DIRS))


VPATH := $(SOURCEDIRS)

all: hex bin size

hex: $(TARGET).hex
elf: $(TARGET).elf
bin: $(TARGET).bin

%.hex: %.elf
	@printf "  OBJCOPY $(*).hex\n"
	$(Q)$(OBJCOPY) -Oihex $(*).elf $(*).hex

%.bin: %.elf
	@printf "  OBJCOPY $(*).bin\n"
	$(Q)$(OBJCOPY) -Obinary $(*).elf $(*).bin

$(TARGET).elf: $(OBJS) $(LDSCRIPT)
	@printf "  LD      $@\n"
	$(Q)$(LD) $(LDFLAGS) $(ARCH_FLAGS) $(OBJS) $(LDLIBS) -o $@

%.o: %.c
	@printf "  CC      $(<F)\n"
	$(Q)$(CC) $(CFLAGS) -MD -o $@ -c $<

clean:
	@printf "  CLEAN\n"
	$(Q)rm -f *.elf *.bin *.hex *.srec *.list *.map $(SDIR)/*.o $(SDIR)/*.d

flashloader: $(TARGET).hex
	$(FLASHLOADER) -c -pn 

size:
	@echo "--------------------------"
	@$(SZ) $(TARGET).elf

include $(wildcard $(addsuffix /*.d, $(SOURCEDIRS)))

.PHONY: clean elf hex bin size

-include flasher.mk

